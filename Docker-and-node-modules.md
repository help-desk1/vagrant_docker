# Docker & node-modules

- Let's say we have the following Dockerfile and docker-compose.yml files for an express application...

```
FROM node:10.15-slim

ENV NODE_ENV=production

WORKDIR /app

COPY package.json package-lock*.json ./

RUN npm install && npm cache clean --force

COPY . .

CMD ["node", "./bin/www"]
```

```
version: '2.4'

services:
  express:
    build: .
    ports:
      - 3000:3000
    volumes:
      - .:/app
    environment:
      - DEBUG=sample-express:*
```

- It's a standard Dockerfile for a node application that copies package* files, installs all of the project dependencies, and runs the application. So what happens when you run
`docker-compose up`... That application immediately crashes because it cannot find any of it's dependencies. Why? Didn't we run `npm install` in our Dockerfile? Yes, but then we created a bind mount in our compose file mounting all of our application code. And if we did not have node_modules present locally it will not be present inside the container.

To solve this, we first need to run `docker-compose run express npm install` before starting our container. This will install our app dependencies inside the container and will also populate our bind mount, so we will now see a node_modules folder locally as well.

Now, running `docker-compose up` starts our express app successfully.
