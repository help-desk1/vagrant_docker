# Vagrant

To run vagrant, run the below commands. To run docker, "Enable" the below features.

- bcdedit /set hypervisorlaunchtype off (Powershell)
- dism.exe /Online /Disable-Feature:HypervisorPlatform (Powershell)
- dism.exe /Online /Disable-Feature:VirtualMachinePlatform (Powershell)
- systeminfo.exe (Powershell)


# Docker

## Build an image

- Build and tag an image using a Dockerfile
    - docker build -t image-name:latest .

## Run an image

- Run an image in the background
    - docker run -d image-name

## Stop a container

- Stop a running container
    - docker stop container-id

## Remove a container

- Stop and force remove a running container
    - docker rm -f container-id

## Tag an image

- Tag an image
    - docker tag image-to-use ctwillie/image-tagged:latest

## Push an image to Docker Hub

- Push a tagged image to Docker Hub
    - docker push ctwillie/image-name:latest

## Create a volume

- Create a named volume
    - docker volume create volume-name

## Use a volume

- Mount a named volume to persist data
    - docker run -d -v named-volume-name:/to/path/in/container ubuntu

- Mount a bind mount to a container
    - docker run -d -v /host/path/to/mount:/to/path/in/container ubuntu
    - docker run -d -v "$(pwd):/app" ubuntu


## Inspect a volume

- Inspect a named volume
    - docker volume inspect volume-name


## Watch container logs

- Watch logs of a detached container
    - docker logs -f container-id


## Examples

- Bind mount a node application and install dependencies
    - docker run -dp 3000:3000 \
    -w /app -v "$(pwd):/app" \
    node:12-alpine \
    sh -c "yarn install && yarn run dev"















